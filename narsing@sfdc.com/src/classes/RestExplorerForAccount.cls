@RestResource(urlMapping='/v1/Account/*')
global with sharing class RestExplorerForAccount {
@HttpPost
global static AccountWrapper doPost(String name,String phone,String website)
{
    RestRequest req=RestContext.request;
    RestResponse res=RestContext.response;
    AccountWrapper response=new AccountWrapper();
    Account a=new Account();
    a.Name=name;
    a.Phone=phone;
    a.Website=website;
    insert a;
    response.accList.add(a);
    response.status='Success';
    response.message='Your account was created successfully.';
    return response;
}
@HttpGet    
global static AccountWrapper goGet()
{
    RestRequest req=RestContext.request;
    RestResponse res=RestContext.response;
    AccountWrapper response=new AccountWrapper();
    String accountId=req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
    system.debug('...accountId.....'+accountId);
    if(doSearch(accountId))
    {
        searchAccounts(req,res,response);
    }
    else
    {
        findAccounts(res,response,accountId);
    }
    return response;
}
 private static boolean doSearch(String accountId)
 {
     if(accountId=='accounts')
     {
         return true;
        
     }
     else
     {
        return false; 
     }
 }
 private static void searchAccounts(RestRequest req,RestResponse res,AccountWrapper aWrapper)
 {
     String srchTerm=req.params.get('Name');
     if(srchTerm==null || srchTerm=='')
     {
         aWrapper.status='error';
         aWrapper.message='You mus provide name of search Term.';
         res.statusCode=400;
     }
     else
     {
         String searchTxt='%'+srchTerm+'%';
         List<Account> acctList=[select id,name,phone,website from Account where name Like:searchTxt];
         if(acctList!=null && acctList.size()>0)
         {
          aWrapper.accList=acctList;
          aWrapper.status='Success';
          aWrapper.message=acctList.size()+'Accounts were found that matched your search term.';
         }
         else
         {
             aWrapper.status='error';
             aWrapper.message='No Accounts were fount based on that name.please search again.';
         }
         system.debug('......aWrapper.......'+aWrapper);
      }
   
 }
private static void findAccounts(RestResponse res,AccountWrapper aWrapper,String accountId)
{
    if(accountId!=null && accountId!='')
    {
        List<Account> result=[select id,name,phone,website from Account where Id=:accountId];
        if(result.size()>0 && result!=null)
        {
          aWrapper.accList.add(result[0]);
          aWrapper.status='Success';
        }
        else
        {
           aWrapper.status='error';
           aWrapper.message='The account could not be found,please try again.';
           res.statusCode=404;
        }
    }
    else
    {
        aWrapper.status='error';
        aWrapper.message='You must specify an external Id.';
        res.statusCode=400;
    }
    
}
global class AccountWrapper
{
  public List<Account> accList;
  public String status;
  public String message;
    public AccountWrapper()
    {
        accList=new List<Account>();
    }
}
}