trigger AccountFutureAvoidInsideLoops on Account (after insert,after update) {
if(trigger.isAfter)
{
if(trigger.isInsert || trigger.isUpdate)
{
for(Account a:trigger.new)
{
asynApex.processFutureMethod(a.id);
}
}
}
}