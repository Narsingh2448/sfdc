public class homeController {

    public String endpointUrl { get; set; }

    public String LOGIN_DOMAIN { get; set; }

    public PageReference loginProcess() {
      HttpRequest request = new HttpRequest();
     // request.setEndpoint('https://' + LOGIN_DOMAIN + '.salesforce.com/services/Soap/u/30.0');
     request.setEndpoint(endpointUrl);
     request.setMethod('POST');
        request.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        request.setHeader('SOAPAction', '""');
        request.setBody('<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/"><Header/><Body><login xmlns="urn:partner.soap.sforce.com"><username>' + uname+ '</username><password>' + pwd+ '</password></login></Body></Envelope>');
        Dom.XmlNode resultElmt = (new Http()).send(request).getBodyDocument().getRootElement()
            .getChildElement('Body', 'http://schemas.xmlsoap.org/soap/envelope/')
            .getChildElement('loginResponse', 'urn:partner.soap.sforce.com')
            .getChildElement('result', 'urn:partner.soap.sforce.com');
        final String SERVER_URL = resultElmt.getChildElement('serverUrl', 'urn:partner.soap.sforce.com') .getText().split('/services')[0];
        final String SESSION_ID = resultElmt.getChildElement('sessionId', 'urn:partner.soap.sforce.com') .getText();
        
        system.debug('@@@@@@' + SESSION_ID);
        return null;
    }


    public String pwd { get; set; }

    public String uname { get; set; }
 public boolean displayPopup {get; set;}    
   
    public void closePopup()
    {       
        displayPopup = false;   
    }    
    public void showPopup()
    {       
        displayPopup = true;   
    }   
}