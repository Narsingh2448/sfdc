<apex:page >
<body>
<ul>Single Sign-On
<li>
Users to access all authorized network resources without having to separately log in to each resource.Single Sign-On also enables your organization to 
integrate with an external identity management system or perform web-based single sign-on to Force.com. Single Sign-On enables multiple types of authentication integration, but the most common are:

<ul>
 <li>Use an existing directory of user names and passwords, such as Active Directory or LDAP, for Salesforce users</li>
<li>Allow seamless sign-on to Force.com applications, eliminating the need for explicit user log on actions</li>
</ul>
</li>
<li>
<ul>Benifits:Reduction in administrative costs, increased ease of use and better implementation of security schemes.
<li>Reduced Administrative Costs: With Single Sign-On, all user authentication information resides in a central directory, which reduces the need to maintain, monitor and potentially synchronized multiple stores, as well as reducing user support requests around passwords.</li>
<li>Increased ease of use: Each user only has a single username and password which grants them access to corporate resources and Salesforce. Reduced complexity means an easier to use environment that provides seamless access to all resources. Single Sign-On also saves users time, since each individual sign-on process can take 5 to 20 seconds to complete. And removing the extra step of logging into Salesforce can increase user adoption of your Salesforce applications by lowering the barrier to use</li>
<li>Increased Security: Any password policies that you have established for your corporate network will also be in effect for Salesforce. In addition, sending an authentication credential that is only valid for a single use can increase security for users who have access to sensitive data.</li>
</ul>
</li>
<li>
<ul>How Single Sign-On Works
<li>When a user tries to log in—either online or using the API—Salesforce validates the username and checks the user’s profile settings. </li>
<li> If the user’s profile has the "Uses Single Sign-on" user permission, then Salesforce does not authenticate the username with the password. Instead, a Web Services call is made to the user’s single sign-on service, asking it to validate the username and password. </li>
<li> The Web Services call passes the username, password, and sourceIp to a Web Service defined for your organization. (sourceIp is the IP address that originated the login request). You must create and deploy an implementation of the Web Service that can be accessed by Salesforce.com servers. </li>
<li> Your implementation of the Web Service validates the passed information and returns either "true" or "false." </li>
<li> If the response is "true," then the login process continues, a new session is generated, and the user proceeds to the application. If "false" is returned, then the user is informed that his or her username and password combination was invalid.</li>
</ul>
</li>
<li>
<ul>Enabling Single Sign-On
<li>Contact Salesforce.com to turn on Single Sign-On for your organization.</li>
<li><ul>Build your SSO Web Service:<li>Download the Web Services Description Language (WSDL) file, AuthenticationService.wsdl, that describes the Single Sign-On service. It can be used to automatically generate a server-side stub to which you can add your specific implementation. For example, in the WSDL2Java tool from Apache Axis, you can use the --server-side switch. In the wsdl.exe tool from .NET, you can use the /server switch. The WSDL is available within Salesforce by clicking Setup | Integrate | API | Delegated Authentication WSDL.</li></ul></li>
<li>In Salesforce, specify your organization’s Single Sign-On Gateway URL by clicking Setup | Security Controls | Single Sign-On settings. </li>
<li>Modify your user profiles to contain the "Uses Single Sign-On" user permission. In Salesforce, click Setup | Manage Users | Profiles to add or edit profiles. It is recommended you create a new user with a new profile to test single sign on. Do not test with the administrator account.</li>
</ul>
</li>
<li>
<ul>Sample SOAP
<li><apex:image id="theImage" value="{!$Resource.SSO_Saop}" width="700" height="400"/></li>

</ul>
</li>
<li>
In the previous sections of this discussion, you have learned how to enable Salesforce to support Single Sign-On capabilities. The actual implementation of Single Sign-On is transparent to users, but involves a number of steps behind the scenes.
When you configure Salesforce for Single Sign-On, you are allowing a delegated authentication authority. When a user first logs onto their network environment, they are initially authenticated by this authority. When the user attempts to log on to subsequent protected applications, instead of passing a user name and password to the application, the user requests a token from a token generator. (On Windows, this token request can use the NTLM protocol.) The received token is passed to the application, which verifies that the token properly identifies the user, and then allows the user access to the application.
Salesforce can use this method, since the password field is simply used to exchange information with the client, rather than specifying a particular data type. This flexibility means that Salesforce can accept a token, which is then used with the delegated authentication authority to verify the user. If the verification succeeds, the user is logged on to Salesforce. If the verification fails, the user receives an error. The process flow for Salesforce Single Sign-On is shown in the figure below.
Typically, this Single Sign-On process is initiated by means of a link on a corporate intranet page. The link requests the token, passes it to the Salesforce login page, and accepts the result of the login attempt from Salesforce.
You can configure the Salesforce delegated authentication authority to only allow tokens, or to accept either tokens or passwords. If the authority only accepts tokens, a Salesforce user cannot log onto Salesforce directly, since they cannot create a valid token. However, many companies choose to allow both tokens and passwords. In this environment, a user could still log onto Salesforce through the login page, or use a link to implement Single Sign-On for the user.

</li>
<li>
<ul>Best Practices for Implementing Single Sign-On
<li>Your organization’s implementation of the Web Service must be accessible by salesforce.com servers. Typically this means that you must deploy the Web Service on a server in your DMZ. Remember to use your server’s external DNS name when entering the Single Sign-On Gateway URL on the Company Information page within Salesforce.</li>
<li>If the Salesforce.com server cannot connect to your server, or the request takes longer than 4 seconds to process, the login attempt will fail. An error will be reported to the user indicating that his or her corporate authentication service is down.</li>
<li>Namespaces and element names are case sensitive in SOAP messages. Wherever possible, generate your server stub from the WSDL to ensure accuracy.</li>
<li>For security reasons, you should make your service available by SSL only. You must use an SSL certificate from a trusted provider, such as Verisign or Thawte. For a full list of trusted providers,<a href="https://developer.salesforce.com/page/Outbound_Messaging_SSL_CA_Certificates"> see this link </a>.</li>
<li>sourceIp is the IP address that originated the login request. Use this information to restrict access based on the user’s location. Note that the Salesforce feature that validates login IP ranges continues to be in effect for Single Sign-On users.</li>
<li>You need a way to map your organization’s internal usernames and Salesforce usernames. If your organization does not follow a standard mapping, you may be able to extend your user database schema (for example, Active Directory) to include the Salesforce username as an attribute of a user account. Your authentication service can then use this attribute to map back to a user account. Alternatively, you can use a database to store the mapping of Salesforce username to your directory's username</li>
<li>Do not enable Single Sign-On for the system administrator’s profile. If your system administrators were Single Sign-On users and your Single Sign-On server had an outage, they would have no way to log in to Salesforce. System administrators should always be able to log in to Salesforce so they can disable Single Sign-On in the event of a problem.</li>
<li>We recommend that you use a Developer Edition account when developing a Single Sign-On solution before implementing it in your organization. To sign up for a free Developer Edition account, go to register or login.</li>
<li>Make sure to test your implementation with Salesforce clients such as Outlook Edition, Office Edition, and Offline Edition. For more information on these clients, see the Salesforce online help. For information on making single sign on work with these clients,<a href="http://developer.force.com/cookbook/recipe/implementing-single-sign-on-for-clients">see this document.</a></li>
</ul>
</li>
<li>
<ul>Frequently Asked Questions
<li><ul>How do I enable Single Sign-On? <li>Log a case with customer support and ask for the feature to be enabled for your organization. Note that this only enables the ability to configure Single Sign-On - you must either implement a solution as described in this document or purchase a partner solution</li></ul></li>
<li><ul>Where in Salesforce do I configure Single Sign-On? <li>The WSDL is available by clicking Setup | Integrate | API.
You can specify your organization’s Single Sign-On Gateway URL at Setup | Company Profile | Company Information.
Click Setup | Manage Users | Profiles to add or edit profiles; associate profiles with the “Uses Single Sign-On” user permission with your Single Sign-On users.</li></ul></li>
<li><ul>How are passwords reset when Single Sign-On has been implemented? <li>Password reset is disabled for Single Sign-On users because Salesforce no longer manages their passwords. Users who try to reset their passwords in Salesforce will be directed to their Salesforce administrator.</li></ul></li>
<li><ul>Where can I view Single Sign-On login errors? <li>Administrators with the "Modify All Data" permission can view the twenty-one most recent Single Sign-On login errors for your organization by clicking Setup | Manage Users | Single Sign-On Error History. For each failed login, you can view the user's username, login time, and the error.</li></ul></li>
<li><ul>Does Single Sign-On work outside my corporate firewall? <li>Yes, Single Sign-On can work outside your corporate firewall. When users are outside the corporate firewall, they can use their network passwords to log in to Salesforce. Alternately, you can require that users must first be connected to your corporate network in order to log in, or you could build a custom login page that required multi-factor authentication if users are outside the corporate firewall.</li></ul></li>
<li><ul>Can I configure a start page and logout page that are specific to my company? <li>Yes - please refer to the sample implementations for details on ssoStartPage and logoutUrl.</li></ul></li>
<li><ul>Does Salesforce support SAML tokens? <li>SAML tokens can be used with this solution described in this document, with the delegated authentication listener validating the token. Salesforce.com also release native SAML support with the Summer 08 release.</li></ul></li>
<li><ul>Can Single Sign-On work with Offline Edition? <li>Single Sign-On can work with Offline Edition if Single Sign On is set up to work with both tokens and passwords. In the offline use case, end users should use their network password to access offline edition.</li></ul></li>
</ul>
</li>
<li>
<ul>Practice
<li><a href="http://developer.force.com/cookbook/recipe/implementing-single-sign-on-for-clients">CookBook</a></li>
<li>
<ul>Implement SAML based Single Sign On (SSO) | Using Salesforce as Identity Provider (Idp) as well as Service Provider (SP)
   <li>SAML based Authentication in Salesforce(Security Assertion Markup Language)(is an XML-based, open-standard data format for exchanging authentication and authorization data between parties, in particular, between an identity provider and a service provider. SAML is a product of the OASIS Security Services Technical Committee)
   <ul>addressing the browser SSO<li>SAML(Security Assertion Markup Language)</li><li> OpenID Connect protocol</li></ul>
   </li>
</ul>
</li>
</ul>
</li>
</ul>

</body>
</apex:page>