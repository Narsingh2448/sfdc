public with sharing class ZipController {

    private ApexPages.StandardController controller;
    public List<String> paths {get;set;}
    public ZipController(ApexPages.StandardController controller) {
        this.controller=controller;
        List<Attachment> attachments=[select name from Attachment where ParentId=:controller.getId()];
        paths=new List<String>();
        for(Attachment att:attachments)
        {
        paths.add(att.name);
        }
    }
    
    @RemoteAction
    public static String getZipFileEntry(String path, String state)
    {
    return null;
    }
}