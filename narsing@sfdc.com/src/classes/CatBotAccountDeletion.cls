public class CatBotAccountDeletion {
public class ChatBotTempOutput{
        @InvocableVariable(required= true)
        public String Name ;

        @InvocableVariable(required= true)
        public String Temp ;
    }

    public class ChatBotTempInput{
        @InvocableVariable(required= true)
        public String Name ;
               
    }

    @InvocableMethod(Label ='deleteAccount' description='return temp')
    public static List<ChatBotTempOutput> deleteRecord(List<ChatBotTempInput> input)
    { 
        List<ChatBotTempOutput> lstOutput = new List<ChatBotTempOutput>();
        List<Account> accList=new List<Account>();
        
        Set<String> accNames=new Set<String>();
        
        for( ChatBotTempInput ChatBI : input){
             
             
            accNames.add(ChatBI.Name);
                      }
       for(Account a:[SELECT Name FROM Account WHERE Name in :accNames])
        {
            ChatBotTempOutput obj = new ChatBotTempOutput();
             obj.Name = a.Name;
             obj.Temp = 'success1'; // You can make some callout to get real Tem
             lstOutput.add(obj);
accList.add(a);
        }
        if(accList.size()>0 && accList!=null)
        {
            delete accList;
        }
         return  lstOutput;      
    }
}