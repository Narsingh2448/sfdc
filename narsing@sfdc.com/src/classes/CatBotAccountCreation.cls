public class CatBotAccountCreation {
public class ChatBotTempOutput{
        @InvocableVariable(required= true)
        public String Name ;

        @InvocableVariable(required= true)
        public String Temp ;
    }

    public class ChatBotTempInput{
        @InvocableVariable(required= true)
        public String Name ;
        @InvocableVariable(required= true)
        public Integer customerID;
    }

    @InvocableMethod(Label ='createAccount' description='return temp')
    public static List<ChatBotTempOutput> createRecord(List<ChatBotTempInput> input)
    {
         List<ChatBotTempOutput> lstOutput = new List<ChatBotTempOutput>();
         List<Account> accList=new List<Account>();
         for( ChatBotTempInput ChatBI : input){
             Account acc=new Account();
             ChatBotTempOutput obj = new ChatBotTempOutput();
             obj.Name = ChatBI.Name;
             obj.Temp = 'success'; // You can make some callout to get real Tem
             lstOutput.add(obj);
             acc.Name=ChatBI.Name;
             acc.Customer_ID__c=ChatBI.customerID;
             accList.add(acc);
         }
        insert accList;
         return  lstOutput;      
    }
}