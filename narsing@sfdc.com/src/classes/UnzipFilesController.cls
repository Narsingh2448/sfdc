public class UnzipFilesController {

    public PageReference upload() {
    
    ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO,'Uploading...'));
    // Metadata Service
    NarsingMetadata.MetadataImport service=createService();
    // Map existing
     List<NarsingMetadata.ListMetadataQuery> queries = new List<NarsingMetadata.ListMetadataQuery>();        
     NarsingMetadata.ListMetadataQuery queryStaticResource = new NarsingMetadata.ListMetadataQuery();
     queryStaticResource.type_x = 'StaticResource';
     queries.add(queryStaticResource);    
     NarsingMetadata.FileProperties[] fileProperties = service.listMetadata(queries, Double.valueOf(39.0)); 
   system.debug('.............queries........................'+queries);
   system.debug('..fileProperties .....'+fileProperties);
   Map<String, NarsingMetadata.FileProperties> mapFileProperties = new Map<String, NarsingMetadata.FileProperties>(); 
   for(NarsingMetadata.FileProperties fileProperty:fileProperties)
   {
   mapFileProperties.put(fileProperty.fullName,fileProperty);
   }
   System.debug('............mapFileProperties..............'+mapFileProperties);
   // Uploads the file as a Static Resource
   NarsingMetadata.StaticResource staticResource = new NarsingMetadata.StaticResource();
        staticResource.fullName = RESOURCE_NAME;
        staticResource.contentType = 'application/zip';
        staticResource.cacheControl = 'private';
        staticResource.description = 'Temporary upload to unzip file for user ' + UserInfo.getName();
        staticResource.content = EncodingUtil.base64Encode(Content); 
        System.debug('.........staticResource...............'+staticResource);
        
      AsyncResult = service.create(new List<NarsingMetadata.Metadata> { staticResource })[0];
     // NarsingMetadata.SaveResult [] results = service.createMetadata(new NarsingMetadata.Metadata[] { staticResource });
        System.debug('...Async....result....'+AsyncResult);
        return null;
    }
    public static String RetrieveSeesionId()
    {
    
    HttpRequest request = new HttpRequest();
        request.setEndpoint('https://' + LOGIN_DOMAIN + '.salesforce.com/services/Soap/u/30.0');
        request.setMethod('POST');
        request.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        request.setHeader('SOAPAction', '""');
        request.setBody('<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/"><Header/><Body><login xmlns="urn:partner.soap.sforce.com"><username>' + userName+ '</username><password>' + pwd+ '</password></login></Body></Envelope>');
        Dom.XmlNode resultElmt = (new Http()).send(request).getBodyDocument().getRootElement()
            .getChildElement('Body', 'http://schemas.xmlsoap.org/soap/envelope/')
            .getChildElement('loginResponse', 'urn:partner.soap.sforce.com')
            .getChildElement('result', 'urn:partner.soap.sforce.com');
        final String SERVER_URL = resultElmt.getChildElement('serverUrl', 'urn:partner.soap.sforce.com') .getText().split('/services')[0];
        final String SESSION_ID = resultElmt.getChildElement('sessionId', 'urn:partner.soap.sforce.com') .getText();
        
        system.debug('@@@@@@' + SESSION_ID);
      serverURL= SERVER_URL ; 
    
    return SESSION_ID;
    }
    public static  NarsingMetadata.MetadataImport createService()
    {
    String s=RetrieveSeesionId();
    System.debug('.......s.........'+s);
    System.debug('.......serverURL.............'+serverURL);
    NarsingMetadata.MetadataImport service =new NarsingMetadata.MetadataImport();
    //service.endpoint_x = URL.getSalesforceBaseUrl().toExternalForm() + '/services/Soap/m/39.0';
    service.endpoint_x = serverURL+'/services/Soap/m/30.0' ;
    service.SessionHeader=new NarsingMetadata.SessionHeader_element();
    //service.SessionHeader.sessionId=UserInfo.getSessionId();
    
    service.SessionHeader.sessionId=s;
   system.debug('....service.......'+service);
    return service;
    }
    public transient Blob Content {get; set;}
    private static String RESOURCE_NAME = 'upload'+UserInfo.getUserId();
    public NarsingMetadata.AsyncResult AsyncResult {get; private set;} 
    public String response{get;set;}
        public String accName{get;set;}
        public String accPhone{get;set;}
        public String accWebsite{get;set;}
        
        static String LOGIN_DOMAIN = 'na46';
        public static String pwd = 'satyam123amrWrchVq4riM8tSZHNT6DhW';
        public static String userName = 'narsing@bh.com';
        public static String serverURL;
}