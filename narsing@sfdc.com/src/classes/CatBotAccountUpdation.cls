public class CatBotAccountUpdation {
public class ChatBotTempOutput{
        @InvocableVariable(required= true)
        public String Name ;

        @InvocableVariable(required= true)
        public String Temp ;
    }

    public class ChatBotTempInput{
        @InvocableVariable(required= true)
        public String Name ;
         @InvocableVariable(required= true)
        public Integer customerID ;
        
    }

    @InvocableMethod(Label ='udateAccount' description='return temp')
    public static List<ChatBotTempOutput> udateRecord(List<ChatBotTempInput> input)
    {
        Map<String,Integer> accMap=new Map<String,Integer>();
         List<ChatBotTempOutput> lstOutput = new List<ChatBotTempOutput>();
         List<Account> accList=new List<Account>();
        Set<String> accNames=new Set<String>();
         for( ChatBotTempInput ChatBI : input){
            accNames.add(ChatBI.Name);
            accMap.put(ChatBI.Name, ChatBI.customerID);
            }
        for(Account a:[SELECT Name FROM Account WHERE Name in :accNames])
        {
            a.Customer_ID__c=accMap.get(a.Name);
            accList.add(a);
            ChatBotTempOutput obj = new ChatBotTempOutput();
             obj.Name = a.Name;
             obj.Temp = 'success'; // You can make some callout to get real Tem
             lstOutput.add(obj);
        }
        
        update accList;
         return  lstOutput;      
    }
}